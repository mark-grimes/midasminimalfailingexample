# Minimal failing example of a potential Midas bug

This example frontend is to show up a potential issue in [Midas](https://bitbucket.org/tmidas/midas). It runs
fine but crashes if compiled with an address sanitizer. Note that Midas itself also has be compiled with the
address sanitizer.

To compile Midas with the sanitizer I added the following command line parameters when configuring CMake:
```
-DCMAKE_C_FLAGS="-fsanitize=address" -DCMAKE_CXX_FLAGS="-fsanitize=address"
```

To compile this example, set `MIDASSYS` correctly and call `./build.sh`, which adds the sanitizer flag.

To run, start the resulting `minimalMidasFrontend` and change the watched value in the ODB. I.e. use
Midas' web frontend to toggle the value of `/DebugTest/SomeValues/boolValue`. This will trigger the watch
callback and the address sanitizer should kill the program with a stack trace.

This is the stack trace I get when compiled against Midas commit [f3364636](https://bitbucket.org/tmidas/midas/src/f33646363dbf86f846464f4015c05f8d8c6c838d):

```
Frontend name          :     foo
Event buffer size      :     0
User max event size    :     0
# of events per buffer :     0

Connect to experiment Mu3e...
OK
Init hardware...
OK
Value of boolValue has changed
=================================================================
==43410==ERROR: AddressSanitizer: heap-use-after-free on address 0x000105d02854 at pc 0x00010227c08c bp 0x00016dd54030 sp 0x00016dd54028
WRITE of size 4 at 0x000105d02854 thread T0
    #0 0x10227c088 in midas::odb::set_last_index(int) odbxx.h:946
    #1 0x10226d064 in midas::u_odb::operator bool() odbxx.cxx:1689
    #2 0x102347f10 in bool midas::odb::get<bool>()+0x56c (minimalMidasFrontend:arm64+0x10029ff10)
    #3 0x102345760 in midas::odb::operator bool<bool, (bool*)0>()+0x320 (minimalMidasFrontend:arm64+0x10029d760)
    #4 0x1023452c8 in callbackFunction(midas::odb&)+0x1a0 (minimalMidasFrontend:arm64+0x10029d2c8)
    #5 0x10234d504 in decltype(std::declval<void (*&)(midas::odb&)>()(std::declval<midas::odb&>())) std::__1::__invoke[abi:v160006]<void (*&)(midas::odb&), midas::odb&>(void (*&)(midas::odb&), midas::odb&)+0x5c (minimalMidasFrontend:arm64+0x1002a5504)
    #6 0x10234d474 in void std::__1::__invoke_void_return_wrapper<void, true>::__call<void (*&)(midas::odb&), midas::odb&>(void (*&)(midas::odb&), midas::odb&)+0x1c (minimalMidasFrontend:arm64+0x1002a5474)
    #7 0x10234d448 in std::__1::__function::__alloc_func<void (*)(midas::odb&), std::__1::allocator<void (*)(midas::odb&)>, void (midas::odb&)>::operator()[abi:v160006](midas::odb&)+0x20 (minimalMidasFrontend:arm64+0x1002a5448)
    #8 0x102349224 in std::__1::__function::__func<void (*)(midas::odb&), std::__1::allocator<void (*)(midas::odb&)>, void (midas::odb&)>::operator()(midas::odb&)+0x20 (minimalMidasFrontend:arm64+0x1002a1224)
    #9 0x10228f0b0 in std::__1::__function::__value_func<void (midas::odb&)>::operator()[abi:v160006](midas::odb&) const function.h:510
    #10 0x10223eac8 in std::__1::function<void (midas::odb&)>::operator()(midas::odb&) const function.h:1156
    #11 0x10223e924 in midas::odb::watch_callback(int, int, int, void*) odbxx.cxx:96
    #12 0x10222ff24 in db_update_record_local(int, int, int, int) odb.cxx:13589
    #13 0x1020d91b4 in cm_dispatch_ipc(char const*, int, int) midas.cxx:5409
    #14 0x1022b5ed0 in ss_suspend_process_ipc(int, int, int) system.cxx:4362
    #15 0x1022b4c40 in ss_suspend(int, int) system.cxx:4697
    #16 0x1020dc3c0 in cm_yield(int) midas.cxx:5695
    #17 0x10233d098 in scheduler() mfe.cxx:2375
    #18 0x10232e688 in main mfe.cxx:2719
    #19 0x1a066ff24  (<unknown module>)

0x000105d02854 is located 52 bytes inside of 104-byte region [0x000105d02820,0x000105d02888)
freed by thread T0 here:
    #0 0x1029f952c in wrap__ZdlPv+0x74 (libclang_rt.asan_osx_dynamic.dylib:arm64e+0x6152c)
    #1 0x102279bf0 in midas::u_odb::~u_odb() odbxx.cxx:1482
    #2 0x102241da4 in midas::u_odb::~u_odb() odbxx.cxx:1478
    #3 0x10224b728 in midas::odb::~odb() odbxx.h:538
    #4 0x10224b7c0 in midas::odb::~odb() odbxx.h:537
    #5 0x102345cd4 in frontend_init()+0x41c (minimalMidasFrontend:arm64+0x10029dcd4)
    #6 0x10232e478 in main mfe.cxx:2679
    #7 0x1a066ff24  (<unknown module>)

previously allocated by thread T0 here:
    #0 0x1029f90ec in wrap__Znwm+0x74 (libclang_rt.asan_osx_dynamic.dylib:arm64e+0x610ec)
    #1 0x102258720 in midas::odb::read_key(std::__1::basic_string<char, std::__1::char_traits<char>, std::__1::allocator<char>> const&) odbxx.cxx:561
    #2 0x1022720e8 in midas::odb::connect(std::__1::basic_string<char, std::__1::char_traits<char>, std::__1::allocator<char>> const&, std::__1::basic_string<char, std::__1::char_traits<char>, std::__1::allocator<char>> const&, bool, bool) odbxx.cxx:1240
    #3 0x102272dd8 in midas::odb::connect(std::__1::basic_string<char, std::__1::char_traits<char>, std::__1::allocator<char>>, bool, bool) odbxx.cxx:1276
    #4 0x102345c94 in frontend_init()+0x3dc (minimalMidasFrontend:arm64+0x10029dc94)
    #5 0x10232e478 in main mfe.cxx:2679
    #6 0x1a066ff24  (<unknown module>)

SUMMARY: AddressSanitizer: heap-use-after-free odbxx.h:946 in midas::odb::set_last_index(int)
Shadow bytes around the buggy address:
  0x000105d02580: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x000105d02600: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x000105d02680: fa fa fa fa fa fa fa fa 00 00 00 00 00 00 00 00
  0x000105d02700: 00 00 00 00 00 fa fa fa fa fa fa fa fa fa 00 00
  0x000105d02780: 00 00 00 00 00 00 00 00 00 00 00 fa fa fa fa fa
=>0x000105d02800: fa fa fa fa fd fd fd fd fd fd[fd]fd fd fd fd fd
  0x000105d02880: fd fa fa fa fa fa fa fa fa fa fd fd fd fd fd fd
  0x000105d02900: fd fd fd fd fd fd fd fa fa fa fa fa fa fa fa fa
  0x000105d02980: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 fa fa
  0x000105d02a00: fa fa fa fa fa fa 00 00 00 00 00 00 00 00 00 00
  0x000105d02a80: 00 00 00 00 fa fa fa fa fa fa fa fa fa fa fa fa
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
==43410==ABORTING
```
