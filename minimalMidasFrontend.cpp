#include "midas.h"
#include "odbxx.h"
#include "mfe.h"

void callbackFunction( midas::odb& myOdb )
{
    printf( "Value of %s has changed\n", myOdb.get_name().c_str() );

    if( myOdb )
    {
        printf("Value is now true\n");
    }
    else
    {
        printf("Value is now false\n");
    }
}

INT frontend_init(void)
{
    midas::odb tempOdb = {
        { "boolValue", false },
    };
    tempOdb.connect("/DebugTest/SomeValues");

    tempOdb.watch( &callbackFunction );

    return CM_SUCCESS;
}

const char *frontend_name = "foo";
const char *frontend_file_name = __FILE__;
BOOL frontend_call_loop = false;
INT max_event_size = 0;
INT max_event_size_frag = 0;
INT event_buffer_size = 0;
INT display_period = 0;
BOOL equipment_common_overwrite = false;
EQUIPMENT equipment[] = { {} };
INT frontend_exit(void) { return CM_SUCCESS; }
INT frontend_loop(void) { return CM_SUCCESS; }
INT begin_of_run(INT run_number, char *error) { return CM_SUCCESS; }
INT end_of_run(INT run_number, char *error) { return CM_SUCCESS; }
INT pause_run(INT run_number, char *error) { return CM_SUCCESS; }
INT resume_run(INT run_number, char *error) { return CM_SUCCESS; }
INT poll_event(INT source, INT count, BOOL test) { return CM_SUCCESS; }
INT interrupt_configure(INT cmd, INT source, POINTER_T adr) { return CM_SUCCESS; }
